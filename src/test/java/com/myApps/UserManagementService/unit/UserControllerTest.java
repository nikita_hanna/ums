package com.myApps.UserManagementService.unit;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myApps.UserManagementService.domain.AddressEntity;
import com.myApps.UserManagementService.domain.UserEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void getUsers() throws Exception {
		 mockMvc.perform(get("/getUsers"))
        .andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.*", hasSize(10)))
        .andReturn().getResponse();
	}
	
	@Test
	public void getUserWithIdReturningBadRequest()throws Exception{
		mockMvc.perform(get("/getUser/id/{id}","test"))
        .andExpect(status().isBadRequest())
		.andExpect(jsonPath("$.errorMessage", is("User id is expected to be of numerical type!")))
		.andExpect(jsonPath("$.errorCode", is("BAD_REQUEST")));
	}
	
	@Test
	public void getUserWithIdReturningSuccess()throws Exception{
		mockMvc.perform(get("/getUser/id/{id}",5))
        .andExpect(status().isOk())
		.andExpect(jsonPath("$.firstName", is("Alison")))
		.andExpect(jsonPath("$.id", is(5)));
	}
	
	@AfterAll
	public void saveUserTest()throws Exception{
		UserEntity user = new UserEntity(5L, "Mr","Lillian","Gerald","Male",1234,new AddressEntity(5L, "35 Harrow Road","Liverpool","NSW","2170"));
		mockMvc.perform(post("/saveUser")
		.contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(user)))
        .andExpect(status().isOk())
		.andExpect(jsonPath("$.firstName", is("Lillian")))
		.andExpect(jsonPath("$.address.street", is("35 Harrow Road")));
	}
	
	
}
