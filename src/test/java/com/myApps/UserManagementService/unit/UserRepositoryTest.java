package com.myApps.UserManagementService.unit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.myApps.UserManagementService.domain.AddressEntity;
import com.myApps.UserManagementService.domain.UserEntity;
import com.myApps.UserManagementService.repositories.UserRepository;


@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenFindAll(){
        UserEntity user = createUser("Mr","Lillian","Gerald","Male",1234,createAddress("35 Harrow Road","Liverpool","NSW","2170"));
        entityManager.persistAndFlush(user);
        UserEntity user2 = createUser("Ms","Julie","Smith","Female",1834,createAddress("15 Hill Road","Schofields","NSW","2180"));
        entityManager.persistAndFlush(user2);

        List<UserEntity> users = userRepository.findAll();
        System.out.println("Size: "+ users.size());
        assertEquals(12, users.size());
        assertEquals(user,users.get(10));
    }

    @Test
    public void whenFindById(){
        UserEntity user = createUser("Mr","Lillian","Gerald","Male",1234,createAddress("35 Harrow Road","Liverpool","NSW","2170"));
        entityManager.persistAndFlush(user);

        Optional<UserEntity> testUser = userRepository.findById(user.getId());

        assertThat(testUser.get().getFirstName()).isEqualTo(user.getFirstName());

    }
    
    private UserEntity createUser(String title, String firstName, String lastName, String gender,long employerId, AddressEntity address) {
	    UserEntity user = new UserEntity();
	    user.setTitle(title);
	    user.setEmployerId(employerId);
	    user.setFirstName(firstName);
	    user.setLastName(lastName);
	    user.setGender(gender);
	    user.setAddress(address);
	    return user;
    }
    
    private AddressEntity createAddress(String street, String city, String state, String postcode) {
    	AddressEntity address = new AddressEntity();
    	address.setCity(city);
    	address.setState(state);
    	address.setStreet(street);
    	address.setPostcode(postcode);
	    return address;
    }

}
