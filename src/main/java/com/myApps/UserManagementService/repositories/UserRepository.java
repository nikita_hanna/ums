package com.myApps.UserManagementService.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.myApps.UserManagementService.domain.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long>{

}
