package com.myApps.UserManagementService.rest;

import com.myApps.UserManagementService.domain.AddressEntity;
import com.myApps.UserManagementService.services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AddressController {

    @Autowired
    AddressService addressService;

    @RequestMapping( value = "/addresses", method = RequestMethod.GET)
    public List<AddressEntity> getAllAddresses(){
        return addressService.getAllAddress();
    }

    @RequestMapping( value = "/saveAddress", method = RequestMethod.POST)
    public AddressEntity saveAddress(@RequestBody AddressEntity address){
        return addressService.save(address);
    }
}
