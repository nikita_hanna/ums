package com.myApps.UserManagementService.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.myApps.UserManagementService.domain.UserEntity;
import com.myApps.UserManagementService.exception.ValidationException;
import com.myApps.UserManagementService.services.UserService;

@RestController
public class UserController {

	UserService userService;
    @Autowired
    public UserController(UserService userService) {
    	this.userService = userService;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getGreetings(){
        return "Welcome to User Management Service";
    }

    @RequestMapping( value ="/getUsers", method = RequestMethod.GET)
    public List<UserEntity> getUsers(){
        return userService.getAllUsers();
    }

    @RequestMapping( value = "/getUser/id/{id}", method = RequestMethod.GET)
    public UserEntity getUserWithId(@PathVariable("id") String userId){
        try {
            return userService.getUserWithId(Long.parseLong(userId));
        }catch (NumberFormatException ex){
            throw new ValidationException("User id is expected to be of numerical type!");
        }
    }

    @RequestMapping( value = "/saveUser", method = RequestMethod.POST)
    public UserEntity saveUser(@RequestBody UserEntity user){
        return userService.saveUser(user);
    }

    @RequestMapping( value = "/deleteUser", method = RequestMethod.DELETE)
    public void deleteUser(@RequestBody UserEntity user){
        userService.deleteUser(user);
    }


}
