package com.myApps.UserManagementService.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ExceptionResponse> resourceNotFound(ResourceNotFoundException ex){
        ExceptionResponse exResponse = new ExceptionResponse(ex.getMessage(),"NOT_FOUND");
        return new ResponseEntity<ExceptionResponse>(exResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionResponse> validationException(ValidationException ex){
        ExceptionResponse exResponse = new ExceptionResponse(ex.getMessage(),"BAD_REQUEST");
        return new ResponseEntity<ExceptionResponse>(exResponse, HttpStatus.BAD_REQUEST);
    }
}
