package com.myApps.UserManagementService.services;

import java.util.List;

import com.myApps.UserManagementService.domain.UserEntity;

public interface UserService {


    public List<UserEntity> getAllUsers();

    public UserEntity getUserWithId(long id);

    public UserEntity saveUser(UserEntity user);

    public void deleteUser(UserEntity entity);


}
