package com.myApps.UserManagementService.services.impl;

import com.myApps.UserManagementService.domain.UserEntity;
import com.myApps.UserManagementService.exception.ResourceNotFoundException;
import com.myApps.UserManagementService.repositories.UserRepository;
import com.myApps.UserManagementService.services.AddressService;
import com.myApps.UserManagementService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {


    @Autowired
    UserRepository userRepository;

    @Autowired
    AddressService addressService;

    @Override
    public List<UserEntity> getAllUsers(){
        return userRepository.findAll();
    }

    @Override
    public UserEntity getUserWithId(long id){
        Optional<UserEntity> user = userRepository.findById(id);
        if(user.isPresent())
            return user.get();
        else
            throw new ResourceNotFoundException("User not found!");
    }

    @Override
    public UserEntity saveUser(UserEntity user){
        return userRepository.save(user);

    }

    @Override
    public void deleteUser(UserEntity entity){
        userRepository.delete(entity);
    }
}
