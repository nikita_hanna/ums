package com.myApps.UserManagementService.services.impl;

import com.myApps.UserManagementService.domain.AddressEntity;
import com.myApps.UserManagementService.repositories.AddressRepository;
import com.myApps.UserManagementService.services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AddressServiceImpl implements AddressService {

    @Autowired
    AddressRepository addressRepository;


    @Override
    public Optional<AddressEntity> getAddress(long addressId) {
        return addressRepository.findById(addressId);
    }

    @Override
    public List<AddressEntity> getAllAddress() {
        return addressRepository.findAll();
    }

    @Override
    public AddressEntity save(AddressEntity address) {

        return addressRepository.save(address);
    }
}
