package com.myApps.UserManagementService.services;

import com.myApps.UserManagementService.domain.AddressEntity;

import java.util.List;
import java.util.Optional;

public interface AddressService {

    public Optional<AddressEntity> getAddress(long addressId);
    public List<AddressEntity> getAllAddress();
    public AddressEntity save(AddressEntity address);
}
