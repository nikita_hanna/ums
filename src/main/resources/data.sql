DROP TABLE IF EXISTS USERS;
DROP TABLE IF EXISTS ADDRESSES;

CREATE TABLE ADDRESSES(
  address_id INT AUTO_INCREMENT PRIMARY KEY,
  street VARCHAR(250),
  city VARCHAR(250),
  state VARCHAR(250),
  postcode VARCHAR(120)
);

create INDEX aid on ADDRESSES(address_id);

INSERT INTO ADDRESSES (street,city, state, postcode ) VALUES
  ('1 Panorama Avenue','Liverpool', 'NSW', '2170'),('2 Panorama Avenue','Liverpool', 'NSW', '2170'),
  ('3 Panorama Avenue','Liverpool', 'NSW', '2170'),('4 Panorama Avenue','Liverpool', 'NSW', '2170'),
  ('5 Panorama Avenue','Liverpool', 'NSW', '2170'),('6 Panorama Avenue','Liverpool', 'NSW', '2170'),
  ('7 Panorama Avenue','Liverpool', 'NSW', '2170'),('8 Panorama Avenue','Liverpool', 'NSW', '2170'),
  ('9 Panorama Avenue','Liverpool', 'NSW', '2170'),('10 Panorama Avenue','Liverpool', 'NSW', '2170');

CREATE TABLE USERS(
  id INT AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(120),
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  gender VARCHAR(120) NOT NULL,
  employee_id INT NOT NULL,
  address_id Int
);

ALTER TABLE USERS
ADD FOREIGN KEY (address_id) REFERENCES ADDRESSES(address_id);

create INDEX uid on USERS(id);


INSERT INTO USERS (title,first_name, last_name, gender, employee_id ,address_id) VALUES
  ('Mr','Peter', 'Thomas', 'Male',100,1),('Mrs','Sofia', 'Mathews', 'Female',1010,2),
  ('Mr','Gary', 'Hertz', 'Male',1040,3),('Mrs','Emily', 'Rose', 'Female',1060,4),
  ('Ms','Alison', 'Larnes', 'Female',1600,5),('Ms','Mable', 'David', 'Female',1800,6),
  ('Mr','Harry', 'Gomez', 'Male',10220,7),('Ms','Maria', 'Richards', 'Male',1090,8),
  ('Mrs','Adeline', 'Jacobs', 'Female',16700,9),('Mr','Shane', 'Watson', 'Male',1890,10);